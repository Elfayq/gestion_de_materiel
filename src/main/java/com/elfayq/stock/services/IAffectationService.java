package com.elfayq.stock.services;


import java.util.List;

import com.elfayq.stock.entities.Affectation;

public interface IAffectationService {
	
	
	public Affectation save(Affectation entity);

	public Affectation update(Affectation entity);

	public List<Affectation> selectAll();

	public List<Affectation> selectAll(String sortField, String sort);

	public void remove(Long id);
	
	public Affectation getById(Long id);

	public Affectation findOne(String paramName, Object paramValue);

	public Affectation findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

	

}
