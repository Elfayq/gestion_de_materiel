package com.elfayq.stock.services;

import java.util.List;

import com.elfayq.stock.entities.Employer;

public interface IEmployerService {
	
	
	public Employer save(Employer entity);

	public Employer update(Employer entity);

	public List<Employer> selectAll();

	public List<Employer> selectAll(String sortField, String sort);

	public void remove(Long id);

	public Employer getById(Long id);

	public Employer findOne(String paramName, Object paramValue);

	public Employer findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
