package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.IMouvementSockDao;
import com.elfayq.stock.entities.MouvementSock;
import com.elfayq.stock.services.IMouvementSockService;


@Transactional
public class MouvementSockServiceImpl implements IMouvementSockService {
	
	
	private IMouvementSockDao mvmstockDao;

	public void setMvmstockDao(IMouvementSockDao mvmstockDao) {
		this.mvmstockDao = mvmstockDao;
	}

	@Override
	public MouvementSock save(MouvementSock entity) {
		
		return mvmstockDao.save(entity);
	}

	@Override
	public MouvementSock update(MouvementSock entity) {
		
		return mvmstockDao.update(entity);
	}

	@Override
	public List<MouvementSock> selectAll() {
		
		return mvmstockDao.selectAll();
	}

	@Override
	public List<MouvementSock> selectAll(String sortField, String sort) {
		
		return mvmstockDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		mvmstockDao.remove(id);
		
	}

	@Override
	public MouvementSock getById(Long id) {
		
		return mvmstockDao.getById(id);
	}

	@Override
	public MouvementSock findOne(String paramName, Object paramValue) {
		
		return mvmstockDao.findOne(paramName, paramValue);
	}

	@Override
	public MouvementSock findOne(String[] paramNames, Object[] paramValues) {
		
		return mvmstockDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return mvmstockDao.findCountBy(paramName, paramValue);
	}
	
	

}
