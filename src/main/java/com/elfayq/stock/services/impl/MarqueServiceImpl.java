package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.IMarqueDao;
import com.elfayq.stock.entities.Marque;
import com.elfayq.stock.services.IMarqueService;

@Transactional
public class MarqueServiceImpl implements IMarqueService {
	
	private IMarqueDao marqueDao;

	public void setMarqueDao(IMarqueDao marqueDao) {
		this.marqueDao = marqueDao;
	}

	@Override
	public Marque save(Marque entity) {
		
		return marqueDao.save(entity);
	}

	@Override
	public Marque update(Marque entity) {
		
		return marqueDao.update(entity);
	}

	@Override
	public List<Marque> selectAll() {
		return marqueDao.selectAll();
	}

	@Override
	public List<Marque> selectAll(String sortField, String sort) {
		
		return marqueDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		marqueDao.remove(id);
		
	}

	@Override
	public Marque getById(Long id) {
		
		return marqueDao.getById(id);
	}

	@Override
	public Marque findOne(String paramName, Object paramValue) {
		
		return marqueDao.findOne(paramName, paramValue);
	}

	@Override
	public Marque findOne(String[] paramNames, Object[] paramValues) {
		
		return marqueDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return marqueDao.findCountBy(paramName, paramValue);
	}
	
	
	

}
