package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.ILocalDao;
import com.elfayq.stock.entities.Local;
import com.elfayq.stock.services.ILocalService;



@Transactional
public class LocalServiceImpl implements ILocalService {
	
	private ILocalDao localDao;

	public void setLocalDao(ILocalDao localDao) {
		this.localDao = localDao;
	}

	@Override
	public Local save(Local entity) {
		
		return localDao.save(entity);
	}

	@Override
	public Local update(Local entity) {
		
		return localDao.update(entity);
	}

	@Override
	public List<Local> selectAll() {
		
		return localDao.selectAll();
	}

	@Override
	public List<Local> selectAll(String sortField, String sort) {
		
		return localDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		localDao.remove(id);
		
	}

	@Override
	public Local getById(Long id) {
		
		return localDao.getById(id);
	}

	@Override
	public Local findOne(String paramName, Object paramValue) {
		
		return localDao.findOne(paramName, paramValue);
	}

	@Override
	public Local findOne(String[] paramNames, Object[] paramValues) {
		
		return localDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return localDao.findCountBy(paramName, paramValue);
	}
	



}

	