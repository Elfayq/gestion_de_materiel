package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.ICategorieDao;
import com.elfayq.stock.entities.Categorie;
import com.elfayq.stock.services.ICategorieService;


@Transactional
public class CategorieServiceImpl implements ICategorieService {
	
	
	private ICategorieDao catDao;

	public void setCatDao(ICategorieDao catDao) {
		this.catDao = catDao;
	}

	@Override
	public Categorie save(Categorie entity) {
		
		return catDao.save(entity);
	}

	@Override
	public Categorie update(Categorie entity) {
		
		return catDao.update(entity);
	}

	@Override
	public List<Categorie> selectAll() {
		
		return catDao.selectAll();
	}

	@Override
	public List<Categorie> selectAll(String sortField, String sort) {
		
		return catDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		catDao.remove(id);
		
	}

	@Override
	public Categorie getById(Long id) {
		
		return catDao.getById(id);
	}

	@Override
	public Categorie findOne(String paramName, Object paramValue) {
		
		return catDao.findOne(paramName, paramValue);
	}

	@Override
	public Categorie findOne(String[] paramNames, Object[] paramValues) {
		
		return catDao.findOne(paramNames, paramNames);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return catDao.findCountBy(paramName, paramValue);
	}
	
	
	

}
