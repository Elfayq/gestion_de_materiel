package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.IVilleDao;
import com.elfayq.stock.entities.Ville;
import com.elfayq.stock.services.IVilleService;

@Transactional
public class VilleServiceImpl implements IVilleService {
	
	private IVilleDao vilDao;

	public void setVilDao(IVilleDao vilDao) {
		this.vilDao = vilDao;
	}

	@Override
	public Ville save(Ville entity) {
		
		return vilDao.save(entity);
	}

	@Override
	public Ville update(Ville entity) {
		
		return vilDao.update(entity);
	}

	@Override
	public List<Ville> selectAll() {
		
		return vilDao.selectAll();
	}

	@Override
	public List<Ville> selectAll(String sortField, String sort) {
		
		return vilDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		vilDao.remove(id);
		
	}

	@Override
	public Ville getById(Long id) {
		
		return vilDao.getById(id);
	}

	@Override
	public Ville findOne(String paramName, Object paramValue) {
		
		return vilDao.findOne(paramName, paramValue);
	}

	@Override
	public Ville findOne(String[] paramNames, Object[] paramValues) {
		
		return vilDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return vilDao.findCountBy(paramName, paramValue);
	}
	
	

}
