package com.elfayq.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.elfayq.stock.dao.IServiceDao;
import com.elfayq.stock.entities.Service;
import com.elfayq.stock.services.IServiceService;

@Transactional
public class ServiceServiceImpl implements IServiceService{
	
	private IServiceDao servDao;


	

	public void setServDao(IServiceDao servDao) {
		this.servDao = servDao;
	}

	@Override
	public Service save(Service entity) {
		
		return servDao.save(entity);
	}

	@Override
	public Service update(Service entity) {
		
		return servDao.update(entity);
	}

	@Override
	public List<Service> selectAll() {
		
		return servDao.selectAll();
	}

	@Override
	public List<Service> selectAll(String sortField, String sort) {
		
		return servDao.selectAll(sortField, sort);
	}

	@Override
	public void remove(Long id) {
		servDao.remove(id);
		
	}

	@Override
	public Service getById(Long id) {
		
		return servDao.getById(id);
	}

	@Override
	public Service findOne(String paramName, Object paramValue) {
		
		return servDao.findOne(paramName, paramValue);
	}

	@Override
	public Service findOne(String[] paramNames, Object[] paramValues) {
		
		return servDao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return servDao.findCountBy(paramName, paramValue);
	}
	
	

}
