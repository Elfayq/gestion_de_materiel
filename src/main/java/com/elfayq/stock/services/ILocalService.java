package com.elfayq.stock.services;

import java.util.List;

import com.elfayq.stock.entities.Local;

public interface ILocalService {
	
	
	public Local save(Local entity);

	public Local update(Local entity);

	public List<Local> selectAll();

	public List<Local> selectAll(String sortField, String sort);

	public void remove(Long id);

	public Local getById(Long id);

	public Local findOne(String paramName, Object paramValue);

	public Local findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);

}
