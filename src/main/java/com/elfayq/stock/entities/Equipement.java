package com.elfayq.stock.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Equipement implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long equipID;
	private String equipNom;
	private String equipReference;
	private String equipNumSerie;
	private Date equipDateEntree;
	private int equipInventaire;
	
	
	@ManyToOne
	@JoinColumn(name = "CatID")
	private Categorie categories;
	
	
	
	@ManyToOne
	@JoinColumn(name = "MarqID")
	private Marque marques;
	
	
	
	

	


	public Long getEquipID() {
		return equipID;
	}


	public void setEquipID(Long equipID) {
		this.equipID = equipID;
	}


	public String getEquipNom() {
		return equipNom;
	}


	public void setEquipNom(String equipNom) {
		this.equipNom = equipNom;
	}


	public String getEquipReference() {
		return equipReference;
	}


	public void setEquipReference(String equipReference) {
		this.equipReference = equipReference;
	}


	public String getEquipNumSerie() {
		return equipNumSerie;
	}


	public void setEquipNumSerie(String equipNumSerie) {
		this.equipNumSerie = equipNumSerie;
	}


	public Date getEquipDateEntree() {
		return equipDateEntree;
	}


	public void setEquipDateEntree(Date equipDateEntree) {
		this.equipDateEntree = equipDateEntree;
	}


	public int getEquipInventaire() {
		return equipInventaire;
	}


	public void setEquipInventaire(int equipInventaire) {
		this.equipInventaire = equipInventaire;
	}






	public Categorie getCategories() {
		return categories;
	}


	public void setCategories(Categorie categories) {
		this.categories = categories;
	}


	public Marque getMarques() {
		return marques;
	}


	public void setMarques(Marque marques) {
		this.marques = marques;
	}


	public Equipement() {
		
	}
	
	
	
	
	
	
	
	

}
