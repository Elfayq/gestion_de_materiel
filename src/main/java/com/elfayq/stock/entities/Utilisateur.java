package com.elfayq.stock.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Utilisateur implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long UtilisateurID;
	private String UtilisateurNom;
	private String UtilisateurPrenom;
	private String UtilisateurMobile;
	private String UtilisateurMail;
	private String UtilisateurMotdePasse;
	private boolean actived = true;
	

	@OneToMany(mappedBy = "utilisateur")
	List<Role> role;


	public Long getUtilisateurID() {
		return UtilisateurID;
	}


	public void setUtilisateurID(Long utilisateurID) {
		UtilisateurID = utilisateurID;
	}


	public String getUtilisateurNom() {
		return UtilisateurNom;
	}


	public void setUtilisateurNom(String utilisateurNom) {
		UtilisateurNom = utilisateurNom;
	}


	public String getUtilisateurPrenom() {
		return UtilisateurPrenom;
	}


	public void setUtilisateurPrenom(String utilisateurPrenom) {
		UtilisateurPrenom = utilisateurPrenom;
	}


	public String getUtilisateurMobile() {
		return UtilisateurMobile;
	}


	public void setUtilisateurMobile(String utilisateurMobile) {
		UtilisateurMobile = utilisateurMobile;
	}


	public String getUtilisateurMail() {
		return UtilisateurMail;
	}


	public void setUtilisateurMail(String utilisateurMail) {
		UtilisateurMail = utilisateurMail;
	}


	public boolean isActived() {
		return actived;
	}


	public void setActived(boolean actived) {
		this.actived = actived;
	}


	public List<Role> getRole() {
		return role;
	}


	public void setRole(List<Role> role) {
		this.role = role;
	}


	
	public String getUtilisateurMotdePasse() {
		return UtilisateurMotdePasse;
	}


	public void setUtilisateurMotdePasse(String utilisateurMotdePasse) {
		UtilisateurMotdePasse = utilisateurMotdePasse;
	}


	public Utilisateur() {
		
	}
	

	
	
	
	
	
	
}
