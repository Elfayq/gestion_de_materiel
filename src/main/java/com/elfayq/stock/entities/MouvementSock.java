package com.elfayq.stock.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MouvementSock implements Serializable {
	
	
	public static final int ENTRRE =1;
	
	public static final int SORTIE = 2;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long IdMouvement;
	@Temporal(TemporalType.TIMESTAMP)
	private Date DateMouvement;
	
	private BigDecimal Qte;
	private String Commentaire;
	
	
	
	@ManyToOne
	@JoinColumn(name = "equipID")
	private Equipement equipements;



	public MouvementSock() {

	}



	public Long getIdMouvement() {
		return IdMouvement;
	}



	public void setIdMouvement(Long idMouvement) {
		IdMouvement = idMouvement;
	}



	public Date getDateMouvement() {
		return DateMouvement;
	}



	public void setDateMouvement(Date dateMouvement) {
		DateMouvement = dateMouvement;
	}



	public BigDecimal getQte() {
		return Qte;
	}



	public void setQte(BigDecimal qte) {
		Qte = qte;
	}



	public String getCommentaire() {
		return Commentaire;
	}



	public void setCommentaire(String commentaire) {
		Commentaire = commentaire;
	}



	public Equipement getEquipements() {
		return equipements;
	}



	public void setEquipements(Equipement equipements) {
		this.equipements = equipements;
	}



	public static int getEntrre() {
		return ENTRRE;
	}



	public static int getSortie() {
		return SORTIE;
	}
	
	
	
	
	
	

}
