package com.elfayq.stock.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Categorie implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long CatID;
	private String CatNom;
	
	
	@OneToMany (mappedBy="categories")
	private List<Equipement> equipements;


	public Long getCatID() {
		return CatID;
	}


	public void setCatID(Long catID) {
		CatID = catID;
	}


	public String getCatNom() {
		return CatNom;
	}


	public void setCatNom(String catNom) {
		CatNom = catNom;
	}


	public List<Equipement> getEquipements() {
		return equipements;
	}


	public void setEquipements(List<Equipement> equipements) {
		this.equipements = equipements;
	}


	public Categorie() {
		
	}
	

	
	


}
