package com.elfayq.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Service implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ServID;
	private String ServNom;
	
	public Long getServID() {
		return ServID;
	}
	public void setServID(Long servID) {
		ServID = servID;
	}
	public String getServNom() {
		return ServNom;
	}
	public void setServNom(String servNom) {
		ServNom = servNom;
	}
	public Service() {
		
	}
	
	
	
	

}
