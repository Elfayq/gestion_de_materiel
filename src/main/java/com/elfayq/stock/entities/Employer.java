package com.elfayq.stock.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Employer implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long EmpID;
	private String EmpNom;
	private String EmpPrenom;
	private int EmplMatricule;
	private String EmpMobile;
	private String EmpMail;
	
	
	@ManyToOne
	@JoinColumn(name="ServID")
	private Service services;
	
	@OneToMany(mappedBy ="employers")
	List<Affectation> affectations;


	public Employer() {
		
	}


	public Long getEmpID() {
		return EmpID;
	}


	public void setEmpID(Long empID) {
		EmpID = empID;
	}


	public String getEmpNom() {
		return EmpNom;
	}


	public void setEmpNom(String empNom) {
		EmpNom = empNom;
	}


	public String getEmpPrenom() {
		return EmpPrenom;
	}


	public void setEmpPrenom(String empPrenom) {
		EmpPrenom = empPrenom;
	}


	public int getEmplMatricule() {
		return EmplMatricule;
	}


	public void setEmplMatricule(int emplMatricule) {
		EmplMatricule = emplMatricule;
	}


	public String getEmpMobile() {
		return EmpMobile;
	}


	public void setEmpMobile(String empMobile) {
		EmpMobile = empMobile;
	}


	public String getEmpMail() {
		return EmpMail;
	}


	public void setEmpMail(String empMail) {
		EmpMail = empMail;
	}


	public List<Affectation> getAffectations() {
		return affectations;
	}


	public void setAffectations(List<Affectation> affectations) {
		this.affectations = affectations;
	}


	public Service getServices() {
		return services;
	}


	public void setServices(Service services) {
		this.services = services;
	}
	
	
	
	
	
	
	
	
	
	

}
