package com.elfayq.stock.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneAffectation implements Serializable {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long LigneAffectationID;
	
	
	
	@ManyToOne
	@JoinColumn(name="idAffectation")
	private Affectation affectations;
	
	@ManyToOne
	@JoinColumn(name="equipID")
	private Equipement equipements;

	public LigneAffectation() {
		
	}

	public Long getLigneAffectationID() {
		return LigneAffectationID;
	}

	public void setLigneAffectationID(Long ligneAffectationID) {
		LigneAffectationID = ligneAffectationID;
	}

	public Affectation getAffectations() {
		return affectations;
	}

	public void setAffectations(Affectation affectations) {
		this.affectations = affectations;
	}

	public Equipement getEquipements() {
		return equipements;
	}

	public void setEquipements(Equipement equipements) {
		this.equipements = equipements;
	}
	
	
	
	
	

}
