package com.elfayq.stock.dao;

import com.elfayq.stock.entities.Ville;

public interface IVilleDao extends IGenericDao<Ville> {

}
