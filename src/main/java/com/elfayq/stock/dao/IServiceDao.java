package com.elfayq.stock.dao;

import com.elfayq.stock.entities.Service;

public interface IServiceDao extends IGenericDao<Service> {

}
