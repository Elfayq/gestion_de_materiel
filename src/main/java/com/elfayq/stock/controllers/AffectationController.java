package com.elfayq.stock.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.elfayq.stock.services.IAffectationService;
import com.elfayq.stock.services.IEmployerService;
import com.elfayq.stock.services.IEquipementService;
import com.elfayq.stock.services.ILocalService;

@Controller
@RequestMapping(value = "/affectation")
public class AffectationController {
	
	
	@Autowired
	private IAffectationService affectationService;
	
	@Autowired
	private IEquipementService equipementService;
	
	@Autowired
	private IEmployerService employerService;
	
	@Autowired ILocalService localService; 
	
	
	@RequestMapping(value = "/")
	public String index() {
		
		return "affectation/affectation";
	}

}
