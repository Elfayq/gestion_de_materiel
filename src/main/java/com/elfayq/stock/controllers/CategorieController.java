package com.elfayq.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elfayq.stock.entities.Categorie;
import com.elfayq.stock.services.ICategorieService;

@Controller
@RequestMapping(value = "/categorie")
public class CategorieController {
	
	@Autowired
	private ICategorieService categoryService;
	
	
	
	@RequestMapping(value = "/")
	public String categorie(Model model) {

		  List<Categorie> categories = categoryService.selectAll();
		  
		
		if (categories == null) {
			
			categories = new ArrayList<Categorie>();
			
		}
	   model.addAttribute("categories",categories);
	    
	    
		return "categorie/categorie";
		
		
	}
	
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterCategorie(Model model) {
		
		Categorie categorie = new Categorie();
		

		model.addAttribute("categorie", categorie);
	
		
		return "categorie/ajoutercategorie";
		
	}
	
	
	
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerCategorie(Model model, Categorie categorie ) {
		
	if (categorie.getCatID()!= null) {
		categoryService.update(categorie);
			
		} else {
			categoryService.save(categorie);
		}
		
	
		return "redirect:/categorie/";
	}
	
	
	@RequestMapping(value = "/modifier/{CatID}")
	public String modifierCategorie(Model model, @PathVariable Long CatID) {
		
		if (CatID != null) {
			
			Categorie categorie = categoryService.getById(CatID); 
			
			if(categorie != null) {
				 model.addAttribute("categorie",categorie);
			}
		}
		
	
		return "categorie/ajoutercategorie";
	}
	
	
	
	@RequestMapping(value = "/supprimer/{CatID}")
	public String supprimerCategorie(Model model, @PathVariable Long CatID) {
		
		if (CatID != null) {
			
			Categorie categorie = categoryService.getById(CatID);
			if(categorie != null) {
				categoryService.remove(CatID);
			}
		}
		
	
		return "redirect:/categorie/";
	}
	
	
	

}
