package com.elfayq.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.elfayq.stock.entities.Utilisateur;
import com.elfayq.stock.services.IUtilisateurService;

@Controller
@RequestMapping(value = "/utilisateur")
public class UtilisateurController {
	
	
	
	@Autowired
	private IUtilisateurService utilisateurService;
	
	@RequestMapping(value = "/")
	public String utilisateur(Model model) {
		
		List<Utilisateur> utilisateurs = utilisateurService.selectAll();

		
		if (utilisateurs == null) {
			
			utilisateurs = new ArrayList<Utilisateur>();
			
		}
	   model.addAttribute("utilisateurs",utilisateurs);
	    
	    
		return "utilisateur/utilisateur";
		
		
	}
	
	

}
