package com.elfayq.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elfayq.stock.entities.Local;
import com.elfayq.stock.entities.Ville;
import com.elfayq.stock.services.ILocalService;
import com.elfayq.stock.services.IVilleService;

@Controller
@RequestMapping(value = "/local")
public class LocalController {
	
	@Autowired
	private ILocalService localService;
	
	@Autowired
	private IVilleService catLocal;
	
	
	@RequestMapping(value = "/")
	public String local(Model model) {

		  List<Local> locals = localService.selectAll();
		  
		
		if (locals == null) {
			
			locals = new ArrayList<Local>();
			
		}
	   model.addAttribute("locals",locals);
	    
	    
		return "local/local";
		
		
	}
	
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterLocal(Model model) {
		
		Local local = new Local();
		
		List<Ville> villes = catLocal.selectAll();
		if (villes == null) {
			villes = new ArrayList<Ville>();
			
		}
		

		model.addAttribute("local", local);
		
		model.addAttribute("villes",villes);
	
		
		return "local/ajouterlocal";
		
	}
	
	
	
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerLocal(Model model, Local local ) {
		
	if (local.getLocalID()!= null) {
		localService.update(local);
			
		} else {
			localService.save(local);
		}
		
	
		return "redirect:/local/";
	}
	
	
	@RequestMapping(value = "/modifier/{LocalID}")
	public String modifierLocal(Model model, @PathVariable Long LocalID) {
		
		if (LocalID != null) {
			
			Local local = localService.getById(LocalID); 
			
			List<Ville> villes = catLocal.selectAll();
			if (villes == null) {
				villes = new ArrayList<Ville>();
				
			}
			
			model.addAttribute("villes",villes);
			
			if(local != null) {
				 model.addAttribute("local",local);
			}
		}
		
	
		return "local/ajouterlocal";
	}
	
	
	
	@RequestMapping(value = "/supprimer/{LocalID}")
	public String supprimerLocal(Model model, @PathVariable Long LocalID) {
		
		if (LocalID != null) {
			
			Local local = localService.getById(LocalID);
			if(local != null) {
				localService.remove(LocalID);
			}
		}
		
	
		return "redirect:/local/";
	}
	
	
	

}
