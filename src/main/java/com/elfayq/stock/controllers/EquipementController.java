package com.elfayq.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elfayq.stock.entities.Categorie;
import com.elfayq.stock.entities.Equipement;
import com.elfayq.stock.entities.Marque;
import com.elfayq.stock.services.ICategorieService;
import com.elfayq.stock.services.IEquipementService;
import com.elfayq.stock.services.IMarqueService;

@Controller
@RequestMapping(value = "/equipement")
public class EquipementController {
	
	@Autowired
	private IEquipementService equipementService;
	
	@Autowired
	private IMarqueService catMarque;
	
	@Autowired
	private ICategorieService catCategorie;
	
	
	
	@RequestMapping(value = "/")
	public String equipement(Model model) {

		  List<Equipement> equipements = equipementService.selectAll();
		  
		
		if (equipements == null) {
			
			equipements = new ArrayList<Equipement>();
			
		}
	   model.addAttribute("equipements",equipements);
	    
	    
		return "equipement/equipement";
		
		
	}
	
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterEquipement(Model model) {
		
		Equipement equipement = new Equipement();
		
		List<Marque> marques = catMarque.selectAll();
		if (marques == null) {
			marques = new ArrayList<Marque>();
			}
		
		
		List<Categorie> categories = catCategorie.selectAll();
		if (categories == null) {
			categories = new ArrayList<Categorie>();
			}
		
		
		model.addAttribute("equipement", equipement);
		
		model.addAttribute("marques", marques);
		
		model.addAttribute("categories", categories);
		
		return "equipement/ajouterequipement";
		
	}
	
	
	
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerEquipement(Model model, Equipement equipement ) {
		
	if (equipement.getEquipID()!= null) {
		equipementService.update(equipement);
			
		} else {
			equipementService.save(equipement);
		}
		
		//employersService.save(employer);
	
		return "redirect:/equipement/";
	}
	
	
	@RequestMapping(value = "/modifier/{equipID}")
	public String modifierEquipement(Model model, @PathVariable Long equipID) {
		
		if (equipID != null) {
			
			Equipement equipement = equipementService.getById(equipID);
			
			List<Marque> marques = catMarque.selectAll();
			if (marques == null) {
				marques = new ArrayList<Marque>();
				}
			
			model.addAttribute("marques", marques);
			
			
			List<Categorie> categories = catCategorie.selectAll();
			if (categories == null) {
				categories = new ArrayList<Categorie>();
				}
			
			model.addAttribute("categories", categories);
			
			if(equipement != null) {
				 model.addAttribute("equipement",equipement);
			}
		}
		
	
		return "equipement/ajouterequipement";
	}
	
	
	
	@RequestMapping(value = "/supprimer/{equipID}")
	public String supprimerEquipement(Model model, @PathVariable Long equipID) {
		
		if (equipID != null) {
			
			Equipement equipement = equipementService.getById(equipID);
			if(equipement != null) {
				equipementService.remove(equipID);
			}
		}
		
	
		return "redirect:/equipement/";
	}
	
	
	

}
