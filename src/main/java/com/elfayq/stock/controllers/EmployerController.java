package com.elfayq.stock.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.elfayq.stock.entities.Employer;
import com.elfayq.stock.entities.Service;
import com.elfayq.stock.services.IEmployerService;
import com.elfayq.stock.services.IServiceService;

@Controller
@RequestMapping(value = "/employer")
public class EmployerController {
	
	@Autowired
	private IEmployerService employersService;
	
	@Autowired
	private IServiceService catService;
	
	
	@RequestMapping(value = "/")
	public String employer(Model model) {

		  List<Employer> employers = employersService.selectAll();
		  
		
		if (employers == null) {
			
		employers = new ArrayList<Employer>();
			
		}
	   model.addAttribute("employers",employers);
	    
	    
		return "employer/employer";
		
		
	}
	
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterAmployer(Model model) {
		
		Employer employer = new Employer();
		
		List<Service> services = catService.selectAll();
		if (services == null) {
			services = new ArrayList<Service>();
			}
		
		model.addAttribute("employer", employer);
		
		model.addAttribute("services", services);
		
		return "employer/ajouteremployer";
		
	}
	
	
	
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerAmployer(Model model, Employer employer ) {
		
	if (employer.getEmpID()!= null) {
		employersService.update(employer);
			
		} else {
			employersService.save(employer);
		}
		
		//employersService.save(employer);
	
		return "redirect:/employer/";
	}
	
	
	@RequestMapping(value = "/modifier/{EmpID}")
	public String modifierAmployer(Model model, @PathVariable Long EmpID) {
		
		if (EmpID != null) {
			
			Employer employer = employersService.getById(EmpID);
			
			List<Service> services = catService.selectAll();
			if (services == null) {
				services = new ArrayList<Service>();
				}
			
			model.addAttribute("services", services);
			
			if(employer != null) {
				 model.addAttribute("employer",employer);
			}
		}
		
	
		return "employer/ajouteremployer";
	}
	
	
	
	@RequestMapping(value = "/supprimer/{EmpID}")
	public String supprimerAmployer(Model model, @PathVariable Long EmpID) {
		
		if (EmpID != null) {
			
			Employer employer = employersService.getById(EmpID);
			if(employer != null) {
				employersService.remove(EmpID);
			}
		}
		
	
		return "redirect:/employer/";
	}
	
	
	

}
