
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="javascript:void(0);">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">TV LAAYOUNE </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
      <c:url value="/home/" var="home" />
        <a class="nav-link" href="${home }">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span><fmt:message key="common.dashboard"/></span></a>
      </li>
      
            <!-- Divider -->
           <hr class="sidebar-divider my-0">
      
      <li class="nav-item">
      <c:url value="/equipement/" var="equipement" />
        <a class="nav-link" href="${equipement }">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span><fmt:message key="common.equipement"/></span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider my-0">


      <li class="nav-item">
      <c:url value="/affectation/" var="affectation" />
        <a class="nav-link" href="${affectation	}">
          <i class="fas fa-fw fa-cog"></i>
          <span><fmt:message key="common.affectation"/></span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider my-0">


<!-- Nav Item - Tables -->
      <li class="nav-item">
      <c:url value="/stock/" var="stock" />
        <a class="nav-link" href="${stock }">
          <i class="fas fa-fw fa-table"></i>
          <span><fmt:message key="common.stock"/></span></a>
      </li>
      
         <!-- Divider -->
      <hr class="sidebar-divider my-0">   
     

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span><fmt:message key="common.parametrage"/></span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Parametre:</h6>
                  <c:url value="/categorie/" var="categorie" />
                  <c:url value="/marque/" var="marque" />
                  <c:url value="/service/" var="service" />
                  <c:url value="/utilisateur/" var="utilisateur" />
                  <c:url value="/ville/" var="ville" />
                  <c:url value="/local/" var="local" />
                  <c:url value="/employer/" var="employer" />
            
            <a class="collapse-item" href="${categorie }"><fmt:message key="common.parametrage.categorie"/></a>
            <a class="collapse-item" href="${marque }"><fmt:message key="common.parametrage.marque"/></a>          
            <a class="collapse-item" href="${service }"><fmt:message key="common.parametrage.service"/></a>
            <a class="collapse-item" href="${utilisateur }"><fmt:message key="common.parametrage.utilisateur"/></a>
            <a class="collapse-item" href="${ville }"><fmt:message key="common.parametrage.ville"/></a>
            <a class="collapse-item" href="${local }"><fmt:message key="common.parametrage.local"/></a>
            <a class="collapse-item" href="${employer }"><fmt:message key="common.parametrage.employer"/></a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">


     

   <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>