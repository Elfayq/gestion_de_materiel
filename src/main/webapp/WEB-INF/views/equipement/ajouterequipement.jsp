<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Blank</title>

  <!-- Custom fonts for this template-->
  <link href=" <%=request.getContextPath() %>/resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href=" <%=request.getContextPath() %>/resources/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">




    <!-- Sidebar -->
    
    <%@ include file="/WEB-INF/views/menu_left/menu_left.jsp" %>
    
    <!-- End of Sidebar --> 
 
 
 
 

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          
          
          
          
          <!-- Topbar Search -->
          <%@ include file="/WEB-INF/views/menu_header/header.jsp" %>
          
          
                  <!-- End of Topbar -->




        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><fmt:message key="equipement.nouveau"/></h1>

        </div>
        
        
        
        
        
        
        
        <div class="card shadow mb-4">

            <div class="card-body">
              <c:url value="/equipement/enregistrer" var="urlenregistrer"/>
              <f:form modelAttribute="equipement" action="${urlenregistrer }" method="post">
              <f:hidden path="equipID"/>
              <div class="form-group">
               <label><fmt:message key="common.equipNom"/></label>
               <f:input path="equipNom" class="form-control"/>
               </div>
               
               <div class="form-group">
               <label><fmt:message key="common.equipReference"/></label>
               <f:input path="equipReference" class="form-control"/>
               </div>
               
               <div class="form-group">
               <label><fmt:message key="common.equipNumSerie"/></label>
               <f:input path="equipNumSerie" class="form-control"/>
               <p class="help-block">Example block-level help text here.</p>
               </div>
               
               <div class="form-group">
               <label><fmt:message key="common.equipDateEntree"/></label>
               <f:input path="equipDateEntree" class="form-control"/>
               <p class="help-block">Example block-level help text here.</p>
               </div>
               
               <div class="form-group">
               <label><fmt:message key="common.equipInventaire"/></label>
               <f:input path="equipInventaire" class="form-control"/>
               <p class="help-block">Example block-level help text here.</p>
               </div>
               
               <div class="form-group">
               <label><fmt:message key="common.Marques"/></label>
               <f:select class="form-control" path="marques.MarqID" items="${marques }" itemLabel="MarqNom" itemValue="MarqID"/>
               </div>
               
               <div class="form-group">
               <label><fmt:message key="common.Categories"/></label>
               <f:select class="form-control" path="categories.CatID" items="${categories }" itemLabel="CatNom" itemValue="CatID"/>
               </div>
                       
                                     

               <div class="panel-footer">
               <button type="submit" class="btn btn-primary"><i class="fa fa-save"><fmt:message key="common.enregistrer"/></i></button>
               <a href="<c:url value="/equipement/" />" class="btn btn-danger"><i class="fa fa-arrow-left"><fmt:message key="common.annuler"/></i></a>
               </div>
              </f:form>
              
              
              
             
</div>
          </div>
          
          
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


      <!-- Footer -->

<%@ include file="/WEB-INF/views/menu_footer/footer.jsp" %>

      <!-- End of Footer -->
      

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
<%@ include file="/WEB-INF/views/menu_logout/logout.jsp" %>
  




  <!-- Bootstrap core JavaScript-->
  <script src="<%=request.getContextPath() %>/resources/vendor/jquery/jquery.min.js"></script>
  <script src="<%=request.getContextPath() %>/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<%=request.getContextPath() %>/resources/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<%=request.getContextPath() %>/resources/js/sb-admin-2.min.js"></script>

</body>

</html>
